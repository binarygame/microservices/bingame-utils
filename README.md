# bingame-utils

A collection of utilities for the microservices of BinaryGame!

## Getting started

To install the package, run `go get -u 'gitlab.com/binarygame/microservices/bingame-utils'`

## Credits

- Project icon designed by `Uniconlabs` from [Flaticon](https://www.flaticon.com/free-icon/tools_7414098).
