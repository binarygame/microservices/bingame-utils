package bingameutils

import (
	"os"
	"strconv"
)

const (
	/*
	   Used for finding your own service host+port
	*/

	// Current service host address
	serviceHostKey = "SERVICE_HOST"
	// Current service host port
	servicePortKey = "SERVICE_PORT"

	/*
	   Used to discover other services
	*/

	// Host address for the user service
	userServiceHostKey = "USERS_SERVICE_HOST"
	// Port for the user service
	userServicePortKey = "USERS_SERVICE_PORT"
	// Host address for the Valkey service
	valkeyServiceHostKey = "VALKEY_SERVICE_HOST"
	// Port for the Valkey service
	valkeyServicePortKey = "VALKEY_SERVICE_PORT"
	// Host address for the Questions service
	questionsServiceHostKey = "QUESTIONS_SERVICE_HOST"
	// Port for the Questions service
	questionsServicePortKey = "QUESTIONS_SERVICE_PORT"
	// Host address for the Rooms service
	roomsServiceHostKey = "ROOMS_SERVICE_HOST"
	// Port for the Rooms service
	roomsServicePortKey = "ROOMS_SERVICE_PORT"
	// Host address for the Guesses service
	guessesServiceHostKey = "GUESSES_SERVICE_HOST"
	// Port for the Guesses service
	guessesServicePortKey = "GUESSES_SERVICE_PORT"
	// Host address for frontend service
	frontendServiceHostKey = "FRONTEND_SERVICE_HOST"
	// Port for the frontend service
	frontendServicePortKey = "FRONTEND_SERVICE_PORT"
	// Host address for OpenTelemetry collector
	otelCollectorHostKey = "OTEL_COLLECTOR_HOST"
	// Port for the OpenTelemetry collector
	otelCollectorPortKey = "OTEL_COLLECTOR_PORT"
)

// GetServiceAddress returns the full address
//
// If the SERVICE_HOST environment variable is set, it will be used.
// If not, the service address will be set to 0.0.0.0
//
// If the SERVICE_PORT environment variable is set, it will be used.
// If not, the service port will be set to 8000

func GetServiceAddress() (address string) {
	return GetServiceHost() + ":" + strconv.Itoa(GetServicePort())
}

// GetServiceHost returns the service host
//
// If the SERVICE_HOST environment variable is set, it will be used.
// If not, the service address will be set to 0.0.0.0
func GetServiceHost() (address string) {
	serviceHost := os.Getenv(serviceHostKey)
	if serviceHost == "" {
		serviceHost = "0.0.0.0"
	}
	return serviceHost
}

// GetServicePort returns the service port
//
// If the SERVICE_PORT environment variable is set, it will be used.
// If not, the service port will be set to 8000
func GetServicePort() (port int) {
	portText := os.Getenv(servicePortKey)
	port, err := strconv.Atoi(portText)

	if portText == "" || err != nil {
		port = 8000
	}
	return port
}

// GetValkeyAddress returns the Valkey address
//
// If the VALKEY_SERVICE_HOST environment variable is set, it will be used.
// If not, the Valkey address will be set to localhost
//
// If the VALKEY_SERVICE_PORT environment variable is set, it will be used.
// If not, the Valkey port will be set to 6379
func GetValkeyAddress() (address string) {
	return getValkeyHost() + ":" + getValkeyPort()
}

func getValkeyHost() (host string) {
	host = os.Getenv(valkeyServiceHostKey)
	if host == "" {
		host = "localhost"
	}

	return host
}

func getValkeyPort() (port string) {
	port = os.Getenv(valkeyServicePortKey)
	if port == "" {
		port = "6379"
	}

	return port
}

// GetUserServiceAddress returns the User service address.
//
// If the USER_SERVICE_HOST environment variable is set, it will be used.
// If not, the User service address will be set to localhost.
//
// If the USER_SERVICE_PORT environment variable is set, it will be used.
// If not, the User service port will be set to 8001.
func GetUserServiceAddress() (address string) {
	return getUserServiceHost() + ":" + getUserServicePort()
}

func getUserServiceHost() (host string) {
	host = os.Getenv(userServiceHostKey)
	if host == "" {
		host = "localhost"
	}

	return host
}

func getUserServicePort() (port string) {
	port = os.Getenv(userServicePortKey)
	if port == "" {
		port = "8001"
	}

	return port
}

// GetQuestionsServiceAddress returns the Questions service address.
//
// If the QUESTIONS_SERVICE_HOST environment variable is set, it will be used.
// If not, the Questions service address will be set to localhost.
//
// If the QUESTIONS_SERVICE_PORT environment variable is set, it will be used.
// If not, the Questions service port will be set to 8002.
func GetQuestionsServiceAddress() (address string) {
	return getQuestionsServiceHost() + ":" + getQuestionsServicePort()
}

func getQuestionsServiceHost() (host string) {
	host = os.Getenv(questionsServiceHostKey)
	if host == "" {
		host = "localhost"
	}

	return host
}

func getQuestionsServicePort() (port string) {
	port = os.Getenv(questionsServicePortKey)
	if port == "" {
		port = "8002"
	}

	return port
}

// GetRoomsServiceAddress returns the Rooms service address.
//
// If the ROOMS_SERVICE_HOST environment variable is set, it will be used.
// If not, the Rooms service address will be set to localhost.
//
// If the ROOMS_SERVICE_PORT environment variable is set, it will be used.
// If not, the Rooms service port will default set to 8003.
func GetRoomsServiceAddress() (address string) {
	return getRoomsServiceHost() + ":" + getRoomsServicePort()
}

func getRoomsServiceHost() (host string) {
	host = os.Getenv(roomsServiceHostKey)
	if host == "" {
		host = "localhost"
	}

	return host
}

func getRoomsServicePort() (port string) {
	port = os.Getenv(roomsServicePortKey)
	if port == "" {
		port = "8003"
	}

	return port
}

// GetGuessesServiceAddress returns the Guesses service address.
//
// If the GUESSES_SERVICE_HOST environment variable is set, it will be used.
// If not, the Guesses service address will be set to localhost.
//
// If the GUESSES_SERVICE_PORT environment variable is set, it will be used.
// If not, the Guesses service port will be set to 8004.
func GetGuessesServiceAddress() (address string) {
	return getGuessesServiceHost() + ":" + getGuessesServicePort()
}

func getGuessesServiceHost() (host string) {
	host = os.Getenv(guessesServiceHostKey)
	if host == "" {
		host = "localhost"
	}

	return host
}

func getGuessesServicePort() (port string) {
	port = os.Getenv(guessesServicePortKey)
	if port == "" {
		port = "8004"
	}

	return port
}

// GetFrontendServiceAddress returns the Frontend service address.
//
// If the FRONTEND_SERVICE_HOST environment variable is set, it will be used.
// If not, the Frontend service address will be set to localhost.
//
// If the FRONTEND_SERVICE_PORT environment variable is set, it will be used.
// If not, the Frontend service port will be set to 8005.
func GetFrontendServiceAddress() (address string) {
	return getFrontendServiceHost() + ":" + getFrontendServicePort()
}

func getFrontendServiceHost() (host string) {
	host = os.Getenv(frontendServiceHostKey)
	if host == "" {
		host = "localhost"
	}

	return host
}

func getFrontendServicePort() (port string) {
	port = os.Getenv(frontendServicePortKey)
	if port == "" {
		port = "8005"
	}

	return port
}

// GetOtelCollectorServiceAddress returns the OpenTelemtry Collector address.
//
// If the OTEL_COLLECTOR_HOST environment variable is set, it will be used.
// If not, the OpenTelemetry Collector address will be set to localhost.
//
// If the OTEL_COLLECTOR_PORT environment variable is set, it will be used.
// If not, the OpenTelemetry Collector port will be set to 4317.
func GetOtelCollectorAddress() (address string) {
	return getOtelCollectorHost() + ":" + getOtelCollectorPort()
}

func getOtelCollectorHost() (host string) {
	host = os.Getenv(otelCollectorHostKey)
	if host == "" {
		host = "localhost"
	}

	return host
}

func getOtelCollectorPort() (port string) {
	port = os.Getenv(otelCollectorPortKey)
	if port == "" {
		port = "4317"
	}

	return port
}
