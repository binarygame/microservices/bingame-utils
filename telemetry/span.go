package telemetry

import "go.opentelemetry.io/otel/trace"

type Span interface {
	End()
}

type otelSpan struct {
	span trace.Span
}

func (s *otelSpan) End() {
	s.span.End()
}
