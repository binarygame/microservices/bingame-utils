package telemetry

import (
	"context"
	"log"

	"go.opentelemetry.io/otel/metric"
)

type Counter interface {
	Add(ctx context.Context, value interface{}, attrs ...metric.AddOption)
}

type CounterUpDown interface {
	Up(ctx context.Context, value interface{}, attrs ...metric.AddOption)
	Down(ctx context.Context, value interface{}, attrs ...metric.AddOption)
}

type CounterType int

const (
	IntCounterType CounterType = iota
	FloatCounterType
	IntCounterUpDownType
	FloatCounterUpDownType
)

type intCounter struct {
	counter metric.Int64Counter
}

type floatCounter struct {
	counter metric.Float64Counter
}

type intCounterUpDown struct {
	counter metric.Int64UpDownCounter
}

type floatCounterUpDown struct {
	counter metric.Float64UpDownCounter
}

func (c *intCounter) Add(ctx context.Context, value interface{}, attrs ...metric.AddOption) {
	switch v := value.(type) {
	case int:
		c.counter.Add(ctx, int64(v), attrs...)
	case int32:
		c.counter.Add(ctx, int64(v), attrs...)
	case int64:
		c.counter.Add(ctx, v, attrs...)
	default:
		log.Printf("Failed to add value to IntCounter: unexpected type %T\n", value)
	}
}

func (c *floatCounter) Add(ctx context.Context, value interface{}, attrs ...metric.AddOption) {
	if v, ok := value.(float64); ok {
		c.counter.Add(ctx, v, attrs...)
	} else {
		log.Printf("Failed to add value to FloatCounter: expected float64 but got %T\n", value)
	}
}

func (c *intCounterUpDown) Up(ctx context.Context, value interface{}, attrs ...metric.AddOption) {
	switch v := value.(type) {
	case int:
		c.counter.Add(ctx, int64(v), attrs...)
	case int32:
		c.counter.Add(ctx, int64(v), attrs...)
	case int64:
		c.counter.Add(ctx, v, attrs...)
	default:
		log.Printf("Failed to add value to IntCounter: unexpected type %T\n", value)
	}
}

func (c *floatCounterUpDown) Up(ctx context.Context, value interface{}, attrs ...metric.AddOption) {
	if v, ok := value.(float64); ok {
		c.counter.Add(ctx, v, attrs...)
	} else {
		log.Printf("Failed to add value to FloatCounter: expected float64 but got %T\n", value)
	}
}

func (c *intCounterUpDown) Down(ctx context.Context, value interface{}, attrs ...metric.AddOption) {
	switch v := value.(type) {
	case int:
		c.counter.Add(ctx, -int64(v), attrs...)
	case int32:
		c.counter.Add(ctx, -int64(v), attrs...)
	case int64:
		c.counter.Add(ctx, v, attrs...)
	default:
		log.Printf("Failed to add value to IntCounter: unexpected type %T\n", value)
	}
}

func (c *floatCounterUpDown) Down(ctx context.Context, value interface{}, attrs ...metric.AddOption) {
	if v, ok := value.(float64); ok {
		c.counter.Add(ctx, -v, attrs...)
	} else {
		log.Printf("Failed to add value to FloatCounter: expected float64 but got %T\n", value)
	}
}
