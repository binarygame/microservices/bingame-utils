package telemetry

import (
	"context"
	"errors"
	"fmt"
	"log"
	"log/slog"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/sdk/resource"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type TelemetryService interface {
	NewCounter(name, description string, valueType CounterType) error
	NewCounterUpDown(name, description string, valueType CounterType) error
	NewHistogram(name, description string, valueType HistogramType) error
	IncrementCounter(ctx context.Context, name string, value interface{})
	IncrementCounterUpDown(ctx context.Context, name string, value interface{})
	DecrementCounterUpDown(ctx context.Context, name string, value interface{})
	Record(ctx context.Context, name string, properties map[string]string) Span
	ObserveValue(ctx context.Context, name string, value interface{})
}

type telemetryService struct {
	counters   map[string]Counter
	countersUd map[string]CounterUpDown
	histograms map[string]Histogram
	meter      metric.Meter
	tracer     trace.Tracer
}

var serviceName attribute.KeyValue

func New(ctx context.Context, namespace string) (ow *telemetryService, shutdown func(context.Context) error, err error) {
	conn, err := initConn()
	if err != nil {
		log.Fatal(err)
	}

	serviceName = semconv.ServiceNameKey.String(namespace)

	res, err := resource.New(ctx,
		resource.WithAttributes(
			serviceName, // The service name used to display data in backends
		),
	)
	if err != nil {
		log.Fatal(err)
	}

	var shutdownFuncs []func(context.Context) error

	// shutdown calls cleanup functions registered via shutdownFuncs.
	// The errors from the calls are joined.
	// Each registered cleanup will be invoked once.
	shutdown = func(ctx context.Context) error {
		var err error
		for _, fn := range shutdownFuncs {
			err = errors.Join(err, fn(ctx))
		}
		shutdownFuncs = nil
		return err
	}

	// handleErr calls shutdown for cleanup and makes sure that all errors are returned.
	handleErr := func(inErr error) {
		err = errors.Join(inErr, shutdown(ctx))
	}

	// Set up propagator.
	prop := newPropagator()
	otel.SetTextMapPropagator(prop)

	// Set up tracer provider.
	trcShutdown, err := initTracerProvider(ctx, res, conn)
	if err != nil {
		handleErr(err)
		return
	}
	shutdownFuncs = append(shutdownFuncs, trcShutdown)

	// Set up meter provider.
	mtrShutdown, err := initMeterProvider(ctx, res, conn)
	if err != nil {
		handleErr(err)
	}
	shutdownFuncs = append(shutdownFuncs, mtrShutdown)

	ow = &telemetryService{}
	ow.counters = make(map[string]Counter)
	ow.meter = otel.Meter(namespace)
	ow.tracer = otel.Tracer(namespace)

	return
}

func (ow *telemetryService) NewCounter(name, description string, valueType CounterType) error {
	switch valueType {
	case IntCounterType:
		counter, err := ow.meter.Int64Counter(name,
			metric.WithDescription(description),
		)
		if err != nil {
			return fmt.Errorf("failed to create counter: %w", err)
		}
		ow.counters[name] = &intCounter{counter: counter}

	case FloatCounterType:
		counter, err := ow.meter.Float64Counter(name,
			metric.WithDescription(description),
		)
		if err != nil {
			return fmt.Errorf("failed to create counter: %w", err)
		}

		ow.counters[name] = &floatCounter{counter: counter}

	default:
		return errors.New("invalid counter type, must be IntCounterType or FloatCounterType")
	}

	return nil
}

func (ow *telemetryService) NewCounterUpDown(name, description string, valueType CounterType) error {
	switch valueType {
	case IntCounterUpDownType:
		counter, err := ow.meter.Int64UpDownCounter(name,
			metric.WithDescription(description),
		)
		if err != nil {
			return fmt.Errorf("failed to create counter: %w", err)
		}

		ow.countersUd[name] = &intCounterUpDown{counter: counter}

	case FloatCounterUpDownType:
		counter, err := ow.meter.Float64UpDownCounter(name,
			metric.WithDescription(description),
		)
		if err != nil {
			return fmt.Errorf("failed to create counter: %w", err)
		}

		ow.countersUd[name] = &floatCounterUpDown{counter: counter}

	default:
		return errors.New("invalid counter type, must be IntCounterUpDownType or FloatCounterUpDownType")
	}

	return nil
}

func (ow *telemetryService) NewHistogram(name, description string, valueType HistogramType) error {
	switch valueType {
	case IntHistogramType:
		histogram, err := ow.meter.Int64Histogram(name,
			metric.WithDescription(description),
		)
		if err != nil {
			return fmt.Errorf("failed to create counter: %w", err)
		}

		ow.histograms[name] = &intHistogram{his: histogram}

	case FloatHistogramType:
		histogram, err := ow.meter.Float64Histogram(name,
			metric.WithDescription(description),
		)
		if err != nil {
			return fmt.Errorf("failed to create counter: %w", err)
		}

		ow.histograms[name] = &floatHistogram{his: histogram}

	default:
		return errors.New("invalid histogram type, must be IntHistogramType or FloatHistogramType")
	}

	return nil
}

func (ow *telemetryService) IncrementCounter(ctx context.Context, name string, value interface{}) {
	if counter, ok := ow.counters[name]; ok {
		counter.Add(ctx, value)
		return
	}

	slog.Warn("Could not find counter " + name)
}

func (ow *telemetryService) Record(ctx context.Context, name string, properties map[string]string) Span {
	ctx, span := ow.tracer.Start(ctx, name)

	for k, v := range properties {
		span.SetAttributes(attribute.String(k, v))
	}

	return &otelSpan{span: span}
}

func (ow *telemetryService) IncrementCounterUpDown(ctx context.Context, name string, value interface{}) {
	if counter, ok := ow.countersUd[name]; ok {
		counter.Up(ctx, value)
		return
	}

	slog.Warn("Could not find up-down counter " + name)
}

func (ow *telemetryService) DecrementCounterUpDown(ctx context.Context, name string, value interface{}) {
	if counter, ok := ow.countersUd[name]; ok {
		counter.Down(ctx, value)
		return
	}

	slog.Warn("Could not find up-down counter " + name)
}

func (ow *telemetryService) ObserveValue(ctx context.Context, name string, value interface{}) {
	if histogram, ok := ow.histograms[name]; ok {
		histogram.Record(ctx, value)
		return
	}

	slog.Warn("Could not find histogram " + name)
}

// initConn is a helper function that sets up a gRPC connection to the OpenTelemetry Collector.
func initConn() (*grpc.ClientConn, error) {
	// It connects the OpenTelemetry Collector through local gRPC connection.
	conn, err := grpc.NewClient(bingameutils.GetOtelCollectorAddress(),
		// TODO: Add TLS configuration here.
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create gRPC connection to collector: %w", err)
	}

	return conn, err
}
