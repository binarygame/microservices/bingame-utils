package telemetry_test

import (
	"context"
	"testing"

	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
)

func TestCounter(t *testing.T) {
	tw, shutdown, err := telemetry.New(context.Background(), "test-service")

	defer func() {
		if err := shutdown(context.Background()); err != nil {
			t.Fatalf("Failed to shutdown telemetry: %v", err)
		}
	}()

	err = tw.NewCounter("test-counter", "A counter for testing purposes", telemetry.IntCounterType)
	if err != nil {
		t.Fatalf("Failed to create counter: %v", err)
	}

	for i := 0; i < 100; i++ {
		tw.IncrementCounter(context.Background(), "test-counter", 1)
	}

	err = tw.NewCounter("test-counter-float", "A float counter for testing purposes", telemetry.FloatCounterType)
	if err != nil {
		t.Fatalf("Failed to create counter: %v", err)
	}

	for i := 0; i < 100; i++ {
		tw.IncrementCounter(context.Background(), "test-counter-float", 234.64)
	}
}

func BenchmarkCounter(b *testing.B) {
	tw, shutdown, err := telemetry.New(context.Background(), "test-service")

	defer func() {
		if err := shutdown(context.Background()); err != nil {
			b.Fatalf("Failed to shutdown telemetry: %v", err)
		}
	}()

	err = tw.NewCounter("test-counter", "A counter for testing purposes", telemetry.IntCounterType)
	if err != nil {
		b.Fatalf("Failed to create counter: %v", err)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		tw.IncrementCounter(context.Background(), "test-counter", 1)
	}
}
