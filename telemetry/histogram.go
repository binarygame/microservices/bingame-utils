package telemetry

import (
	"context"
	"log"

	"go.opentelemetry.io/otel/metric"
)

type Histogram interface {
	Record(ctx context.Context, value interface{}, attrs ...metric.RecordOption)
}

type HistogramType int

const (
	IntHistogramType HistogramType = iota
	FloatHistogramType
)

type intHistogram struct {
	his metric.Int64Histogram
}

type floatHistogram struct {
	his metric.Float64Histogram
}

func (h *intHistogram) Record(ctx context.Context, value interface{}, attrs ...metric.RecordOption) {
	switch v := value.(type) {
	case int:
		h.his.Record(ctx, int64(v), attrs...)
	case int32:
		h.his.Record(ctx, int64(v), attrs...)
	case int64:
		h.his.Record(ctx, v, attrs...)
	default:
		log.Printf("Failed to add value to IntHistogram: unexpected type %T\n", value)
	}
}

func (h *floatHistogram) Record(ctx context.Context, value interface{}, attrs ...metric.RecordOption) {
	if v, ok := value.(float64); ok {
		h.his.Record(ctx, v, attrs...)
	} else {
		log.Printf("Failed to add value to FloatHistogram: expected float64 but got %T\n", value)
	}
}
