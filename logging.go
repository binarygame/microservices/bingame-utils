package bingameutils

import (
	"log/slog"
	"os"
	"strings"
)

const (
	envKey      = "ENV"
	logLevelKey = "LOG_LEVEL"
)

// Get the logging configuration based on the environment
func getHandlerOptions() *slog.HandlerOptions {
	level := getLogLevel()

	return &slog.HandlerOptions{
		Level:     level,
		AddSource: true,
	}
}

// GetLogger returns a logger based on the environment.
// If the LOG_LEVEL environment variable is set, it will be used to set the log level.
// If not, the log level will be set according to the environment.
//
// The developer does not need to call any sync method as slog handles that automatically.
func GetLogger() *slog.Logger {
	env := os.Getenv(envKey)
	env = strings.TrimSpace(strings.ToLower(env))

	var handler slog.Handler
	options := getHandlerOptions()

	if env == "production" {
		handler = slog.NewJSONHandler(os.Stdout, options)
	} else {
		handler = slog.NewTextHandler(os.Stdout, options)
	}

	return slog.New(handler)
}

// getLogLevel parses the LOG_LEVEL environment variable and returns a slog.Level.
func getLogLevel() slog.Level {
	logLevelRaw := strings.TrimSpace(strings.ToLower(os.Getenv(logLevelKey)))
	var logLevel slog.Level

	switch logLevelRaw {
	case "debug":
		logLevel = slog.LevelDebug
	case "info":
		logLevel = slog.LevelInfo
	case "warn", "warning":
		logLevel = slog.LevelWarn
	case "error":
		logLevel = slog.LevelError
	default:
		logLevel = slog.LevelInfo
	}

	return logLevel
}
