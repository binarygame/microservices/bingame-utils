#!/bin/sh

# Function to recommend package based on distribution
recommend_gum_install() {
  if [ -f /etc/os-release ]; then
    . /etc/os-release
    case "$ID" in
      arch | manjaro)
        echo "Please install gum using: sudo pacman -S gum"
        ;;
      fedora)
        echo "Please install gum using: sudo dnf install gum"
        ;;
      ubuntu | debian)
        echo "Please install gum using: sudo apt-get install gum"
        ;;
      *)
        echo "Please install gum from the appropriate repository for your distribution."
        ;;
    esac
  else
    echo "Unable to detect Linux distribution. Please install gum manually."
  fi
}

# Check if gum is installed
if ! command -v gum >/dev/null 2>&1; then
  echo "gum is not installed."
  recommend_gum_install
  exit 1
fi

# Define the list of microservices and their ports
services=(
  "gateway:8000"
  "users:8001"
  "questions:8002"
  "rooms:8003"
  "guesses:8004"
)

# Use gum to select a service
selected_service=$(printf "%s\n" "${services[@]}" | gum choose --header="Select a microservice: ")

# If a service was selected, split the service and port
if [ -n "$selected_service" ]; then
  echo "selected $selected_service"
  microservice=$(echo "$selected_service" | cut -d':' -f1)
  port=$(echo "$selected_service" | cut -d':' -f2)

  # Connect using Evans
  evans --port "$port" repl -r
else
  echo "No service selected."
fi
