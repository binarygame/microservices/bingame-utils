#!/bin/sh

# Determine the absolute directory where the dash.sh script is located
SCRIPT_DIR=$(dirname "$(realpath "$0")")
DOCKER_COMPOSE_FILE="$SCRIPT_DIR/../docker-compose.yml"
docker compose --file "$DOCKER_COMPOSE_FILE" down cache
docker compose --file "$DOCKER_COMPOSE_FILE" up -d cache
