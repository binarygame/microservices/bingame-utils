#!/bin/sh

# Determine the absolute directory where the dash.sh script is located
SCRIPT_DIR=$(dirname "$(realpath "$0")")

# Function to recommend package based on distribution
recommend_gum_install() {
  if [ -f /etc/os-release ]; then
    . /etc/os-release
    case "$ID" in
      arch | manjaro)
        echo "Please install gum using: sudo pacman -S gum"
        ;;
      fedora)
        echo "Please install gum using: sudo dnf install gum"
        ;;
      ubuntu | debian)
        echo "Please install gum using: sudo apt-get install gum"
        ;;
      *)
        echo "Please install gum from the appropriate repository for your distribution."
        ;;
    esac
  else
    echo "Unable to detect Linux distribution. Please install gum manually."
  fi
}

# Check if gum is installed
if ! command -v gum >/dev/null 2>&1; then
  echo "gum is not installed."
  recommend_gum_install
  exit 1
fi


# Function to display the menu and handle the user's choice
display_menu() {
  choice=$(gum choose "Rebuild Microservices" "Run Evans locally" "Run Evans in bingame-dev" "Check local logs" "Reset Valkey data" "Restart Microservice" "Exit")

  case "$choice" in
    "Rebuild Microservices")
      "$SCRIPT_DIR/buildrun.sh"
      ;;
    "Run Evans locally")
      "$SCRIPT_DIR/evans.sh"
      ;;
    "Run Evans in bingame-dev")
      "$SCRIPT_DIR/dev-evans.sh"
      ;;
    "Check local logs")
      "$SCRIPT_DIR/logs.sh"
      ;;
    "Reset Valkey data")
      "$SCRIPT_DIR/reset-valkey.sh"
      ;;
    "Restart Microservice")
      "$SCRIPT_DIR/restart.sh"
      ;;
    "Exit")
      echo "Exiting..."
      exit 0
      ;;
    *)
      echo "Exiting..."
      exit 0
      ;;
  esac
}

# Loop to continuously display the menu until the user exits
while true; do
  display_menu
done
