#!/bin/sh

SCRIPT_DIR=$(dirname "$(realpath "$0")")
DOCKER_COMPOSE_FILE="$SCRIPT_DIR/../docker-compose.yml"

services=(
  "gateway"
  "users"
  "questions"
  "rooms"
  "guesses"
)

# Use gum to select a service
selected_service=$(printf "%s\n" "${services[@]}" | gum choose --header="Select a microservice: ")

# If a service was selected, split the service and port
if [ -n "$selected_service" ]; then
  echo "selected $selected_service, restarting"  
  docker compose -f "$DOCKER_COMPOSE_FILE" restart --no-deps $selected_service
else
  echo "No service selected."
fi
