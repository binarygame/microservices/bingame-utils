package bingameutils

import (
	"context"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"net/http"
	"strings"

	"github.com/google/uuid"
)

type userCredentialsKey string

type userCredentials struct {
	UserID     string
	PrivateKey string
}

type UserCredentials interface {
	GetID() string
	IsValid() bool
	getPrivateKey() string
}

const privateKeySeedLength = 32

var contextKey userCredentialsKey = "userCredentials"

// UUID5 namespace
var BinGameNamespace = uuid.Must(uuid.Parse("cd9aefbd-4546-405f-9d17-46d0253e7434"))

func (uC *userCredentials) GetID() string {
	return uC.UserID
}

func (uC *userCredentials) IsValid() bool {
	return isUserAuthValid(uC.UserID, uC.PrivateKey)
}

func (uC *userCredentials) getPrivateKey() string {
	return uC.PrivateKey
}

func GetCtxWithCredentialsFromHeader(ctx context.Context, header http.Header) context.Context {
	userCredentials := getUserCredentialsFromRequest(header)

	return context.WithValue(ctx, contextKey, userCredentials)
}

func IsCtxAuthValid(ctx context.Context) bool {
	credentials := getUserCredentialsFromCtx(ctx)
	if credentials == nil {
		return false
	}
	return credentials.IsValid()
}

func GetUserIdFromCtx(ctx context.Context) string {
	creds := getUserCredentialsFromCtx(ctx)
	if creds == nil {
		return ""
	}
	return creds.GetID()
}

func encodeAuthAsBase64(creds *userCredentials) string {
	return base64.StdEncoding.EncodeToString([]byte(creds.UserID + ":" + creds.PrivateKey))
}

// Adds the basic auth header based on the authentication present on context
// Fails silently if the context does not have the credentials set.
func AddAuthToHeaderFromCtx(ctx context.Context, h http.Header) {
	creds := getUserCredentialsFromCtx(ctx)
	if creds == nil {
		return
	}

	// Create b64 encoded auth header
	auth := encodeAuthAsBase64(creds)
	h.Add("Authorization", "Basic "+auth)
}

// GeneratePrivateKey generates a private key
// using random bytes and SHA-256 hash
func GeneratePrivateKey() (string, error) {
	// Generate random bytes
	randomBytes := make([]byte, privateKeySeedLength)
	_, err := rand.Read(randomBytes)
	if err != nil {
		return "", err
	}

	// Calculate the SHA-256 hash
	hashedPrivateKey := sha256.Sum256(randomBytes)

	return hex.EncodeToString(hashedPrivateKey[:]), nil
}

// DerivePublicUUID derives a public UUID from a private key
// using the SHA-256 hash
func DerivePublicUUID(privateKey string) uuid.UUID {
	// Hash the private key using SHA-256
	hash := sha256.Sum256([]byte(privateKey))

	// Create a UUID based on the hash
	uuid := uuid.NewSHA1(BinGameNamespace, hash[:])

	return uuid
}

// IsUserAuthValid checks if the user is authenticated
// by comparing the public UUID derived from the private key
// with the user ID
func isUserAuthValid(userId string, privateKey string) bool {
	return DerivePublicUUID(privateKey).String() == userId
}

// Get the user ID and private key from the auth header
//
// Receives the http header related to the request.
func getUserCredentialsFromRequest(header http.Header) UserCredentials {
	const prefix = "Basic "
	authHeader := header.Get("Authorization")

	if !strings.HasPrefix(authHeader, prefix) {
		return nil
	}

	base64Credentials := strings.TrimPrefix(authHeader, prefix)

	// Decode base64
	auth, err := base64.StdEncoding.DecodeString(base64Credentials)
	if err != nil || len(auth) == 0 {
		return nil
	}

	// Split the auth header into two parts
	// The first part is the user ID
	// The second part is the private key
	credentials := strings.SplitN(string(auth), ":", 2)

	// Check if two parts were found
	if len(credentials) != 2 {
		return nil
	}

	userId := credentials[0]
	privateKey := credentials[1]

	return &userCredentials{UserID: userId, PrivateKey: privateKey}
}

func getUserCredentialsFromCtx(ctx context.Context) *userCredentials {
	if value := ctx.Value(contextKey); value != nil {
		if credentials, ok := value.(*userCredentials); ok {
			return credentials
		}
	}
	return nil
}
