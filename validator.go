package bingameutils

import (
	"errors"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
)

// NewValidator creates a new validator instance and registers custom validations
func NewValidator() *validator.Validate {
	v := validator.New()
	// Register custom validator for room id
	v.RegisterValidation("bingame_room_id", validateRoomID)
	return v
}

func IsValidUUID(u string) bool {
	_, err := uuid.Parse(u)
	return err == nil
}

var (
	ErrRoomIDIncorrectLength   = errors.New("room ID must be exactly 4 characters long")
	ErrRoomIDInvalidCharacters = errors.New("room ID contains invalid characters")
)

// ParseRoomID validates and parses the room ID
func ParseRoomID(s string) (string, error) {
	// Remove spaces and convert to uppercase
	roomID := strings.ToUpper(strings.TrimSpace(s))

	// Check if the room ID is of length 4
	if len(roomID) != 4 {
		return "", ErrRoomIDIncorrectLength
	}

	// Check if all characters in the roomID are in the alphanumericCharset
	for _, char := range roomID {
		if !strings.ContainsRune(RoomIDCharset, char) {
			return "", ErrRoomIDInvalidCharacters
		}
	}

	return roomID, nil
}

// Custom validation function to check if a string is a valid Room ID
func validateRoomID(fl validator.FieldLevel) bool {
	roomID, err := ParseRoomID(fl.Field().String())
	return err == nil && roomID != ""
}
