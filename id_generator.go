package bingameutils

import "math/rand"

// Alphanumeric charset without '0', 'O', 'I' and 'L' to avoid confusion
const RoomIDCharset = "ABCDEFGHJKMNPQRSTUVWXYZ123456789"

// GenerateRandomID generates a random ID of length outLen using the RoomIDCharset
func GenerateRoomID(outLen int) string {
	idBytes := make([]byte, outLen)
	for i := range idBytes {
		idBytes[i] = RoomIDCharset[rand.Intn(len(RoomIDCharset))]
	}

	return string(idBytes)
}
