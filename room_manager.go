// Package bingameutils provides utilities for managing client subscriptions to rooms
// and broadcasting messages to subscribed clients.
package bingameutils

import (
	"math/rand"
	"sync"
	"time"
)

const (
	clientIDByteSize = 32
	broadcastRoomID  = "broadcast-room"
)

// RoomManagerClient represents a single client with an ID and a message channel.
type RoomManagerClient[T any] struct {
	ClientID string // Unique identifier for the client
	Channel  chan T // Channel for sending messages to this client
}

// RoomManagerRoom represents a room with all its subscribed clients.
type RoomManagerRoom[T any] struct {
	RoomID  string
	Clients map[string]*RoomManagerClient[T]
	mu      sync.RWMutex
}

// AddClient adds a client to the room.
func (r *RoomManagerRoom[T]) AddClient(client *RoomManagerClient[T]) {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.Clients[client.ClientID] = client
}

// RemoveClient removes a client from the room.
func (r *RoomManagerRoom[T]) RemoveClient(clientID string) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.Clients, clientID)
}

// Broadcast sends an event to all clients in the room.
func (r *RoomManagerRoom[T]) Broadcast(data T) {
	r.mu.RLock()
	defer r.mu.RUnlock()
	for _, client := range r.Clients {
		select {
		case client.Channel <- data:
			// Message sent successfully
		default:
			// Channel is full, skip this client
		}
	}
}

// RoomManager manages all the rooms and provides methods for room operations.
type RoomManager[T any] struct {
	rooms map[string]*RoomManagerRoom[T]
	mu    sync.RWMutex
}

// NewRoomManager initializes and returns a new RoomManager.
func NewRoomManager[T any]() *RoomManager[T] {
	return &RoomManager[T]{
		rooms: make(map[string]*RoomManagerRoom[T]),
	}
}

// GetOrCreateRoom gets an existing room or creates a new one if it doesn't exist.
func (rm *RoomManager[T]) GetOrCreateRoom(roomID string) *RoomManagerRoom[T] {
	rm.mu.Lock()
	defer rm.mu.Unlock()
	room, exists := rm.rooms[roomID]
	if !exists {
		room = &RoomManagerRoom[T]{
			RoomID:  roomID,
			Clients: make(map[string]*RoomManagerClient[T]),
		}
		rm.rooms[roomID] = room
	}
	return room
}

// GetOrCreateBroadcastRoom gets the special broadcast room that receives events from all channels.
func (rm *RoomManager[T]) GetOrCreateBroadcastRoom() *RoomManagerRoom[T] {
	return rm.GetOrCreateRoom(broadcastRoomID)
}

// Broadcast sends an event to the specified room and the broadcast room.
func (rm *RoomManager[T]) Broadcast(data T, roomID string) {
	room := rm.GetOrCreateRoom(roomID)
	broadcastRoom := rm.GetOrCreateBroadcastRoom()
	go room.Broadcast(data)
	go broadcastRoom.Broadcast(data)
}

// SubscribeClient subscribes a client to a specific room.
func (rm *RoomManager[T]) SubscribeClient(client *RoomManagerClient[T], roomID string) {
	room := rm.GetOrCreateRoom(roomID)
	room.AddClient(client)
}

// UnsubscribeClient removes a client from a specific room.
func (rm *RoomManager[T]) UnsubscribeClient(clientID string, roomID string) {
	rm.mu.RLock()
	room, exists := rm.rooms[roomID]
	rm.mu.RUnlock()
	if exists {
		room.RemoveClient(clientID)
	}
}

// generateClientID generates a random client ID.
func generateClientID() string {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	seededRand := rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, clientIDByteSize)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// NewRoomManagerClient creates and returns a new RoomManagerClient with a random ID and a buffered channel.
func NewRoomManagerClient[T any]() *RoomManagerClient[T] {
	return &RoomManagerClient[T]{
		ClientID: generateClientID(),
		Channel:  make(chan T, 100), // Buffered channel to prevent blocking
	}
}
